<div align="center">
  <h1>Time Guards</h1>
  <p>
    <strong>
     Darmowa gra open-source z mocną deweloperką
    </strong>
  </p>
  <p>
   Time Guards, czas by zmienić świat
  </p>
  <p>

[![Release](https://img.shields.io/badge/release-none-red)](https://codeberg.org/TimeGuards/desktop/releases)
[![License](https://img.shields.io/badge/GPL-3.0_only-green)](https://opensource.org/licenses/GPL-3.0)
[![Version](https://img.shields.io/badge/rustc-1.60.0+-ab6000.svg)]()
<br />
[![Chat on Matrix](https://img.shields.io/badge/chat-on_matrix-51bb9c)](https://matrix.to/#/#timeguards:matrix.org)

  </p>
</div>


## Spis treści

* [O projekcie](#o-projekcie)
* [Rodzaje map](####rodzaje-map)
* [Założenia](####założenia)
* [Zrzuty ekranu](#zrzuty-ekranu)
* [Systemy operacyjne](##systemy-operacyjne)
* [Technologie](##technologie)
* [Budowanie](#budowanie)
* [TODO](#todo)
* [Zalecane oprogramowanie FOSS](#zalecane-oprogramowanie-foss)
* [Zalecenia](#zalecenia)
* [Donacje](#donacje)

* [Licencja](#licencja)

# O projekcie
>

<div align="center">
  <strong>Celem projektu jest stworzenie nowoczesnej rozbudowanej gry zgodnej z ideą FOSS przy użyciu wyłącznie oprogramowania FOSS.</strong>

  Główny wątek fabularny opowiada o mistycznej organizacji strażników czasu.
  Mają oni za zadanie strzec integralności czasoprzestrzeni, która jest regularnie naruszana.
</div>

## Szczegóły fabuły

Gracz rozpoczyna swoją przygodę od spotkania innych strażników czasu podczas okresowego zebrania, w jego trakcie dochodzi do alarmu o załamaniu czasoprzestrzeni. W trybie natychmiastowym gracz wraz z kilkoma strażnikami zostaje wysłany na miejsce zdarzenia.
Na miejscu okazuje się, że do okresu średniowiecznego przez załamanie czasoprzestrzeni przedostał się samochód wraz z przestępcami, którzy uciekając nim przypadkowo wjechali w tunel zakrzywionej czasoprzestrzeni. Przerażeni zaczynają atakować strażników czasu raniąc jednego z nich. W tym momencie gracz uczy się jak wygląda służba strażników, dowiaduje się też o ich zdolnościach magicznych i o tym dlaczego strażnik czasu pomimo swojej potęgi nie może przesadzać z używaniem zdolności przy postronnych osobach nawet gdy one próbują go zabić.

#### Rodzaje map

Ziemia
- Era dinozaurów
- Starożytność
- Średniowiecze
- Nowożytność
- Teraźniejszość
- Przyszłość
- Postapokaliptyczna mapa

#### Założenia
- Darmowa oraz open source (FOSS)
- Stosowanie najnowszysch technologii
- Połączenie róznych gatunków i stylów gier
- Duży kontakt ze społecznością
- Obsługa Steam
- Tryb singleplayer oraz multiplayer (P2P)

# Zrzuty ekranu

## Systemy operacyjne
> Testy gry na systemach operacyjnych

| System operacyjny | Testowane | Działa |
| --- | --- | --- |
| Microsoft Windows 11 | Nie |  |
| Microsoft Windows 10 | Nie |  |
| Debian Bookworm | Nie |  |
| Debian Bullseye | Nie |  |
| Ubuntu Jammy Jellyfish | Nie |  |
| Fedora 36 | Nie |  |
| Fedora 35 | Nie |  |
| [Garuda](https://garudalinux.org/) KDE Dragonized Gaming Edition  | Nie |  |
| [Manjaro](https://manjaro.org/) KDE Plasma 21.2.5 | Nie |  |
| Gentoo | Nie |  |
| FreeBSD 13.0 | Nie |  |
| [Redox OS](https://www.redox-os.org/) 0.6.0 | Nie |  |
| [Vinix](https://github.com/vlang/vinix) | Nie |  |

| Szablony systemów operacyjnych na [Qubes OS](https://www.qubes-os.org/) R4.1 | Testowane | Działa |
| --- | --- | --- |
| Debian Bullseye | Tak | Nie |
| Fedora 34 | Nie |  |
| Arch Linux | Nie |  |
| Gentoo | Nie |  |

| Szablony systemów operacyjnych na [Qubes OS](https://www.qubes-os.org/) R4.0.4 | Testowane | Działa |
| --- | --- | --- |
| Debian Bullseye | Tak | Nie |
| Fedora 34 | Nie |  |
| Arch Linux | Nie |  |

## Technologie

- [Rust](https://www.rust-lang.org/) [ wydanie stabilne - 1.60.0 ] {
  - Silnik [Bevy](https://bevyengine.org/)
}

# Budowanie
> Jak zbudować

```
git clone https://codeberg.org/TimeGuards/desktop
cargo build --release
```

# TODO
>

- Tworzenie menu
- Kreacja głównej postacji
- Tworzenie mapy średniowiecza

# Zalecane oprogramowanie FOSS

| Grafika & Animacje |
| --- |
| [Blender](https://www.blender.org/) |
| [Wings 3D](http://www.wings3d.com/) |
| [GIMP](https://www.gimp.org/) |
| [Krita](https://krita.org/) |
| [Scribus](https://www.scribus.net/) |
| [Synfig](https://www.synfig.org/) |
| [OpenToonz](https://opentoonz.github.io/) |

| Wideo |
| --- |
| [Kdenlive](https://kdenlive.org/) |
| [Shotcut](https://shotcut.org/) |
| [Flowblade](https://jliljebl.github.io/flowblade/) |
| [NATRON](https://natrongithub.github.io/) |

| Audio |
| --- |
| [Audacity](https://www.audacityteam.org/) |
| [Kwave](http://kwave.sourceforge.net/) |
| [LMMS](https://lmms.io/) |

| Projektowanie |
| --- |
| [LibreCAD](https://librecad.org/) |
| [FreeCAD](https://www.freecadweb.org/) |
| [KiCad](https://www.kicad.org/) |

| Programowanie |
| --- |
| [Atom](https://atom.io/) |
| [Neovim](https://neovim.io/) |
| [VSCodium](https://vscodium.com/) |

| Inne |
| --- |

## Zalecenia

- Dodawanie surowych plików poza wynikowymi

# Donacje

## Kryptowaluty

- [Pirate Chain (ARRR)](https://pirate.black/):
>

- [Beam](https://www.beam.mw/):
>

- Monero (XMR):
>

- Dash:
>

- Bitcoin Cash (BCH):
>

- Ethereum (ETH):
>

- Dogecoin (DOGE):
>

- Litecoin (LTC):
>

- Bitcoin (BTC):
>

# Licencja
GPL-3.0 only
