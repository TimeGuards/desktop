<div align="center">
  <h1>Time Guards desktop version</h1>
  <p>
    <strong>
      Free and open-source game with big dev
    </strong>
  </p>
  <p>
   Time Guards, time to change the world
  </p>
  <p>

[![Release](https://img.shields.io/badge/release-none-red)](https://codeberg.org/TimeGuards/desktop/releases)
[![License](https://img.shields.io/badge/GPL-3.0_only-green)](https://opensource.org/licenses/GPL-3.0)
[![Version](https://img.shields.io/badge/rustc-1.60.0+-ab6000.svg)]()
<br />
[![Chat on Matrix](https://img.shields.io/badge/chat-on_matrix-51bb9c)](https://matrix.to/#/#timeguards:matrix.org)

  </p>
</div>


## Table of content
* Languages
** [PL](readme/README.pl.md)
